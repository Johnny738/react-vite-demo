export interface Employee {
  id: number;
  first_name: string;
  last_name: string;
  country: string;
  age: number;
  salary: number;
  position: string;
  imageUrl: string;
}
