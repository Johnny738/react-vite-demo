import { Employee } from "../App";
import { useNavigate } from "react-router-dom";
export default function Card({ props }: { props: Employee }) {
  const navigate = useNavigate();

  const showDetails = () => {
    const path = `/details/${props.id}`;
    navigate(path);
  };
  return (
    <div className="card mb-2" style={{ width: "18rem" }}>
      <img
        className="card-img-top m-auto"
        style={{ width: "200px", height: "200px" }}
        src={props.imageUrl}
        alt="Card image cap"
      />
      <div className="card-body">
        <h5 className="card-title">{props.first_name}</h5>
        <h5 className="card-title">{props.position}</h5>
        <p className="card-text">
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </p>
        <button className="btn btn-primary" onClick={showDetails}>
          Details
        </button>
      </div>
    </div>
  );
}
