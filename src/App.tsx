import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./App.css";
import Card from "./components/Card";
import { Employee } from "./interfaces/Emloyee";

function App() {
  const [data, setData] = useState<Employee[]>([]);
  const navigate = useNavigate();
  useEffect(() => {
    try {
      fetch("http://localhost:9090/employees")
        .then((res) => {
          if (res.ok) {
            return res.json();
          }
          throw new Error("Server error" + res.status);
        })
        .then((res) => setData(res));
    } catch (error: any) {
      throw new Error(error.message);
    }
  }, [data.length]);

  return (
    <>
      <button
        className="btn btn-primary w-50 p-2 mb-3"
        onClick={() => navigate("/details")}
      >
        Create Employee
      </button>
      <div className="container d-flex flex-wrap justify-content-between">
        {data.length != 0 ? (
          data.map((m) => <Card key={m.id} props={m} />)
        ) : (
          <h1>No Employees in the Database</h1>
        )}
      </div>
    </>
  );
}

export default App;
