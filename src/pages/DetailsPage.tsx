import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Employee } from "../interfaces/Emloyee";


export default function DetailsPage() {
  const { id } = useParams();
  const navigate = useNavigate();
  const [employee, setEmployee] = useState<Employee>();

  useEffect(() => {
    if (id) {
      try {
        fetch("http://localhost:9090/employees/" + id)
          .then((res) => res.json())
          .then((data) => setEmployee(data));
      } catch (e: unknown) {
        throw new Error("No employee found");
      }
    }
  }, []);

  //update employee object
  function handleChangefield(e: React.ChangeEvent<HTMLInputElement>) {
    const { id, value } = e.target;

    setEmployee((prev: any) => ({ ...prev, [id]: value }));
  }

  const saveHandler = () => {
    const requesOptions: RequestInit = {
      method: id ? "PUT" : "POST",
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
      body: JSON.stringify(employee),
    };
    try {
      fetch(
        `http://localhost:9090/employees${id ? `/${id}` : ""}`,
        requesOptions
      ).then((res) => {
        if (res.ok) {
          return navigate("/");
        }
        throw new Error("Server error: " + res.status);
      });
    } catch (err: any) {
      console.log(err.message);
    }
  };

  //delete employee
  const deleteEmployee = () => {
    const requesOptions: RequestInit = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
    };
    try {
      fetch("http://localhost:9090/employees/" + id, requesOptions).then(
        (res) => {
          if (res.ok) {
            return navigate("/");
          }
          throw new Error("Server error: " + res.status);
        }
      );
    } catch (err: any) {
      throw new Error(err.message);
    }
  };
  return (
    <div className="container-xl px-4 mt-4">
      <nav className="nav nav-borders">
        <a
          className="nav-link active ms-0"
          href="https://www.bootdey.com/snippets/view/bs5-edit-profile-account-details"
          target="__blank"
        >
          Profile
        </a>
        <a
          className="nav-link"
          href="https://www.bootdey.com/snippets/view/bs5-profile-billing-page"
          target="__blank"
        >
          Billing
        </a>
        <a
          className="nav-link"
          href="https://www.bootdey.com/snippets/view/bs5-profile-security-page"
          target="__blank"
        >
          Security
        </a>
        <a
          className="nav-link"
          href="https://www.bootdey.com/snippets/view/bs5-edit-notifications-page"
          target="__blank"
        >
          Notifications
        </a>
      </nav>
      <hr className="mt-0 mb-4" />
      <div className="row">
        <div className="col-xl-4">
          <div className="card mb-4 mb-xl-0">
            <div className="card-header">Profile Picture</div>
            <div className="card-body text-center">
              <img
                className="img-account-profile rounded-circle img-fluid"
                src={employee?.imageUrl}
                alt=""
              />
              <div className="small font-italic text-muted mb-4">
                JPG or PNG no larger than 5 MB
              </div>
              <button className="btn btn-primary" type="button">
                Upload new image
              </button>
            </div>
          </div>
        </div>
        <div className="col-xl-8">
          <div className="card mb-4">
            <div className="card-header">Account Details</div>
            <div className="card-body">
              <form>
                <div className="row gx-3 mb-3">
                  <div className="col-md-6">
                    <label className="small mb-1">First name</label>
                    <input
                      className="form-control"
                      id="first_name"
                      type="text"
                      placeholder="Enter your first name"
                      onChange={handleChangefield}
                      value={employee?.first_name || ""}
                    />
                  </div>
                  <div className="col-md-6">
                    <label className="small mb-1">Last name</label>
                    <input
                      className="form-control"
                      id="last_name"
                      type="text"
                      placeholder="Enter your last name"
                      onChange={handleChangefield}
                      value={employee?.last_name || ""}
                    />
                  </div>
                </div>
                <div className="row gx-3 mb-3">
                  <div className="col-md-6">
                    <label className="small mb-1">Organization name</label>
                    <input
                      className="form-control"
                      id="position"
                      type="text"
                      placeholder="Enter your organization name"
                      onChange={handleChangefield}
                      value={employee?.position || ""}
                    />
                  </div>
                  <div className="col-md-6">
                    <label className="small mb-1">Country</label>
                    <input
                      className="form-control"
                      id="country"
                      type="text"
                      placeholder="Enter your country"
                      onChange={handleChangefield}
                      value={employee?.country || ""}
                    />
                  </div>
                </div>
                <div className="mb-3">
                  <label className="small mb-1">Image Url</label>
                  <input
                    className="form-control"
                    id="imageUrl"
                    type="email"
                    placeholder="Enter your image Url"
                    onChange={handleChangefield}
                    value={employee?.imageUrl || ""}
                  />
                </div>
                <div className="row gx-3 mb-3">
                  <div className="col-md-6">
                    <label className="small mb-1">Salary</label>
                    <input
                      className="form-control"
                      id="salary"
                      type="tel"
                      placeholder="Enter your salary"
                      onChange={handleChangefield}
                      value={employee?.salary || ""}
                    />
                  </div>
                  <div className="col-md-6">
                    <label className="small mb-1">Age</label>
                    <input
                      className="form-control"
                      id="age"
                      type="text"
                      name="birthday"
                      placeholder="Enter your birthday"
                      onChange={handleChangefield}
                      value={employee?.age || ""}
                    />
                  </div>
                </div>
                <button
                  className="btn btn-primary w-25 p-2"
                  type="button"
                  onClick={saveHandler}
                >
                  Save changes
                </button>
                {id ? (
                  <button
                    className="btn btn-danger ms-4 w-25 p-2"
                    type="button"
                    onClick={deleteEmployee}
                  >
                    Delete
                  </button>
                ) : (
                  " "
                )}
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
